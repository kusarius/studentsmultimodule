name := "StudentsMultiModule"
organization in ThisBuild := "com.underdefense"
scalaVersion in ThisBuild := "2.13.1"

lazy val root = project
  .in(file("."))
  .aggregate(
    common,
    client,
    server
  )

lazy val common = project
  .settings(
    name := "common"
  )

lazy val client = project
  .settings(
    name := "client",
    libraryDependencies ++= Seq()
  )
  .dependsOn(
    common
  )

lazy val server = project
  .settings(
    name := "server",
    libraryDependencies ++= Seq(
      "com.typesafe.akka" %% "akka-http"       % "10.1.11",
      "com.typesafe.akka" %% "akka-stream"     % "2.5.26",
      "de.heikoseeberger" %% "akka-http-circe" % "1.31.0",
      "io.circe"          %% "circe-core"      % "0.12.3",
      "io.circe"          %% "circe-generic"   % "0.12.3",
      "io.circe"          %% "circe-parser"    % "0.12.3"
    )
  )
  .dependsOn(
    common
  )
