import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route

import scala.collection.mutable.ArrayBuffer
import scala.io.StdIn
import io.circe.syntax._
import io.circe.generic.auto._
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport._

import scala.concurrent.ExecutionContextExecutor

object Server {
  implicit val system: ActorSystem = ActorSystem("my-system")
  implicit val executionContext: ExecutionContextExecutor = system.dispatcher

  private val endpointName = "students"
  private val hostname = "192.168.0.106"
  private val port = 8080

  private val students = new ArrayBuffer[Student]

  private val routes: Route =
    path(endpointName) {
      get {
        complete(students.asJson)
      } ~ post { // This tilde '~' is a concat() equivalent.
        entity(as[Student]) { student =>
          // Check if we already have student with the same ID.
          students.indexWhere(_.id == student.id) match {
            case -1 =>
              students.append(student)
              complete(StatusCodes.OK)
            case _ => complete(StatusCodes.BadRequest)
          }
        }
      }
    } ~ pathPrefix(endpointName / IntNumber) { id =>
      get {
        students.find(_.id == id) match {
          case Some(student) => complete(student.asJson)
          case None => complete(StatusCodes.NotFound)
        }
      } ~ put {
        entity(as[Student]) { student =>
          students.indexWhere(_.id == id) match {
            case -1 => complete(StatusCodes.NotFound)
            case index: Int  =>
              students(index) = student
              complete(StatusCodes.OK)
          }
        }
      } ~ delete {
        students.find(_.id == id) match {
          case Some(student) =>
            students -= student
            complete(StatusCodes.OK)
          case None => complete(StatusCodes.NotFound)
        }
      }
    }

  def main(args: Array[String]): Unit = {
    val bindingFuture = Http().bindAndHandle(routes, hostname, port)

    println(s"Server online at http://$hostname:$port\nPress ENTER to stop...")
    StdIn.readLine()

    bindingFuture
      .flatMap(_.unbind())
      .onComplete(_ => system.terminate())
  }
}
